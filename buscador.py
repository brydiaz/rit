from re import A
import manejador_archivos

#Se encarga de buscar en el indice completo que hayan la mayoría de palabras de la busqueda en el resultado
def buscar(consulta):
    arreglo = consulta.lower().split()
    index = manejador_archivos.obtener_index_completo()
    documentos_encontrados = []
    for termino in arreglo:
        for i in index:
            if i['doc_name'] not in documentos_encontrados:
                if  termino in i['palabra']:
                    documentos_encontrados.append(i['doc_name'])
    i = 0
    docs_finales = []
    while i != len(documentos_encontrados):
        f = open('base_datos/coleccion/preprocesado/'+documentos_encontrados[i])
        string = f.readline()
        todo_texto = ""
        while string != "":
            todo_texto += string
            string = f.readline()
        añadir = True
        hayados = 0
        no_hayados = 0
        for j in arreglo:
            if j in todo_texto:
                hayados += 1
            else:  
                no_hayados +=1
        if no_hayados > hayados:
            añadir = False
        if añadir:
            docs_finales.append(documentos_encontrados[i])
        i += 1
    return docs_finales      

