from pydoc import doc
from flask import Flask, render_template, request, flash
import MOVlib
import os 

app = Flask(__name__)
app.secret_key = 'movlib'
DOCS_RELEVANTES = []
NUM_CONSULTA = 0
DOCU_ACTUAL = ''
P_SELECCIONADO = ''

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/volver")
def volver():
    return render_template("index.html")

@app.route('/resultados', methods=['POST','GET']) 
def resultados():
    global DOCS_RELEVANTES
    global NUM_CONSULTA
    global P_SELECCIONADO

    DOCS_RELEVANTES = []
    f = open('base_datos/num_consulta.txt', 'r') #para sobrescribir
    NUM_CONSULTA = int(f.readline())
    f = open('base_datos/num_consulta.txt', 'w') #para sobrescribir
    f.write(str(NUM_CONSULTA+1))
    f.close()
    consulta = request.form['name_input']
    documentos = MOVlib.movlib(consulta)
    print(documentos)

    p_evaluar = request.form['ops']
    P_SELECCIONADO = p_evaluar
    f = open('base_datos/consultas/CONSULTA_'+str(NUM_CONSULTA)+'.txt', 'a') #para sobrescribir
    f.write('CONSULTA: '+consulta+' con un '+p_evaluar+ ' DOCS ANALIZADOS: \n')
    i = 0
    while i != len(documentos):
        if i == len(documentos):
            break
        if i > 20:
            break
        else:
            flash(documentos[i])
            DOCS_RELEVANTES.append([documentos[i],0]) #ES util en 1, 0 no
        i+=1
    i = 0
    if p_evaluar == 'p@5':
        i = 5
    elif p_evaluar == 'p@10':
        i = 10
    else:
        i == 20
    j = 0
    while j != i:
        if j == len(documentos):
            break
        f.write(documentos[j]+'\n')
        j+=1

    return render_template('results.html')



@app.route('/historial', methods=['POST','GET']) 
def historial():
    contenido = os.listdir('base_datos/consultas')
    for consulta in contenido:
        flash(consulta)
    return render_template('history.html')


@app.route('/otra_consulta', methods=['POST','GET']) 
def otra_consulta():
    global P_SELECCIONADO
    global DOCS_RELEVANTES
    if P_SELECCIONADO == 'p@5':
        i = 5
    elif P_SELECCIONADO == 'p@10':
        i = 10
    else:
        i = 20
    j = 0
    importantes = 0
    while j != i:
        if j == len(DOCS_RELEVANTES):
            break
        else:
            if DOCS_RELEVANTES[j][1] == 1:
                importantes += 1
        j += 1
    f = open('base_datos/consultas/CONSULTA_'+str(NUM_CONSULTA)+'.txt', 'a')
    f.write('DOCUMENTOS UTILES '+str(importantes)+':\n')
    j = 0
    while j != i:
        if j == len(DOCS_RELEVANTES):
            break
        else:
            if DOCS_RELEVANTES[j][1] == 1:
                f.write('-'+DOCS_RELEVANTES[j][0]+'\n')
        j += 1
    f.write('PRECISION: '+str(importantes/i))
    f.close()
    return render_template('index.html')

@app.route('/pelicula/<nombre>', methods=['POST','GET']) 
def pelicula(nombre):
    global DOCU_ACTUAL
    DOCU_ACTUAL = nombre
    pelicula = open ('base_datos/coleccion/'+nombre,'r')
    linea = pelicula.readline()
    while linea != '':
        flash(linea)
        linea = pelicula.readline()
    pelicula.close()
    return render_template('movie.html')

@app.route('/historial/<nombre>', methods=['POST','GET']) 
def consulta(nombre):
    consulta = open ('base_datos/consultas/'+nombre,'r')
    linea = consulta.readline()
    while linea != '':
        flash(linea)
        linea = consulta.readline()
    consulta.close()
    return render_template('consult.html')

@app.route('/pelicula/util', methods=['POST','GET']) 
def util():
    global DOCU_ACTUAL
    global DOCS_RELEVANTES
    pelicula = open ('base_datos/coleccion/'+DOCU_ACTUAL,'r')
    linea = pelicula.readline()
    while linea != '':
        flash(linea)
        linea = pelicula.readline()
    pelicula.close()

    for i in DOCS_RELEVANTES:
        if DOCU_ACTUAL == i[0]:
            i[1] = 1
    return render_template('movie.html')

