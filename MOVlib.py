import requests
from datetime import datetime
from bs4 import BeautifulSoup
import descargador
import os
import indexador as ind
import buscador
import sys
#Define como se controla el programa
def movlib(consulta):  
    calendarizador()
    indexador()
    #acá abrimos el web
    return buscador.buscar(consulta)
 
#Obtiene los links para generar el arañador
def arañador():

      base_de_datos = open ('base_datos/links.txt','w')
      base_de_datos.write(str(datetime.today())+"\n")
      referencias = open("links_para_buscar.txt")
      link_a_buscar = referencias.readline()
      peliculas = []
      while link_a_buscar != '':
            print("PELICULAS ESCANEADAS EN: " +link_a_buscar)
            linke = "https://www.rottentomatoes.com/top/bestofrt/top_100_action__adventure_movies/"
            reqs = requests.get(link_a_buscar[:-1])
            soup = BeautifulSoup(reqs.text, 'html.parser')
            tabla = soup.find('table', {'class': 'table'}).find_all('a')
            for pelicula in tabla:
                  if pelicula['href'] not in peliculas:
                        base_de_datos.write('https://www.rottentomatoes.com'+pelicula['href']+"\n")
                        peliculas.append(pelicula['href'])
              
            link_a_buscar = referencias.readline()
      base_de_datos.close()
      referencias.close()

#Verifica si han pasado 7 días para actualizar colección o indexar
def verficar_base():
      
    base_de_datos = open ('base_datos/links.txt','r')   
    fecha_de_busqueda = base_de_datos.readline()
    
    if fecha_de_busqueda == "":  
          base_de_datos.close()
          return True
    else:
          fecha_limpia = ""
          for i in fecha_de_busqueda:
              if i != "\n":
                    fecha_limpia += i
          fecha = datetime.strptime(fecha_limpia, '%Y-%m-%d %H:%M:%S.%f')
          delta = datetime.today() - fecha
          if delta.days > 7:
                base_de_datos.close()
                return True
          else:
                base_de_datos.close()
                return False

#Se encarga de verficar si hay que actualizar o hay un archivo nuevo
def calendarizador():

      if verficar_base():
            print('INICIANDO DESCARGA')
            arañador()
            descargador.coordinador()
      elif verificar_archivo_nuevo():
            print("VERIFICADO ARCHIVOS NUEVOS")
      
      print("LA BASE DE DATOS, ESTA ACTUALIZADA Y LA COLECCIÓN SE MANTIENE ESTABLE")
            
            
            
#Si en la colección no existe un archivo en los links descargados, lo descarga      
def verificar_archivo_nuevo():

      directorio = 'base_datos/coleccion'
      contenido = os.listdir(directorio)
      links = open('base_datos/links.txt','r')
      link = links.readline()
      link = links.readline()
      nombre_links = []
      while link != "":
            nombre_links.append(link[33:].strip())
            link = links.readline()
      control = False
      for i in nombre_links:
            nombre = i[:len(i)] +'.txt'
            if nombre not in contenido:
                  print("NUEVO DETECTADO! EMPEZANDO DESCARGA DE "+ nombre)
                  descargador.descargar_info("https://www.rottentomatoes.com/m/"+i[:len(i)])
                  control = True
      return control

#Ejecuta el indexador en el modulo principal
def indexador():
      if verficar_base():
            print("INICIO DE INDEXADO")
            nombre_archivos = os.listdir("base_datos/coleccion/preprocesado")
            for nombre_documento in nombre_archivos:
                  ind.indexar(nombre_documento)
            ind.calcular_idf_para_los_terminos()
      else: 
            print("AL ESTAR LA BASE ACTUALIZADA NO ES NECESARIO INDEXAR")
