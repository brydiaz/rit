import manejador_archivos
import math
import os
VOCABULARIO = {}
ID = 0

#Recibe el documento y retorna un array con el documento cargado
def cargar_documento_a_arreglo(documento):

    f = open("base_datos/coleccion/preprocesado/"+documento, 'r')
    palabra = f.readline()
    arreglo = []
    while palabra != '':
        arreglo.append(palabra.strip())
        palabra = f.readline()
    return arreglo

#Usa el algoritmo de indexación para crea un indice que es un txt que almacena "diccionarios"
def indexar(nombre_documento):
    global ID
    global VOCABULARIO
    #Se carga el documento al arreglo
    arreglo = cargar_documento_a_arreglo(nombre_documento)
    #Se alimenta el vocabulario
    for palabra in arreglo:
        VOCABULARIO[palabra] = ID
        ID += 1
    #Se cambia el arreglo id:pos
    pos = 0
    for palabra in arreglo:
        id = VOCABULARIO[palabra]
        arreglo[pos] = [id,pos]
        pos += 1
    arreglo.sort(key = lambda x: x[0])   #index 1 means second element
    listas_indexadas = []
    palabras_visitadas = []
    for i in arreglo:
        if i[0] not in palabras_visitadas:
            lista_indexada = []
            lista_indexada.append(nombre_documento)#Nombre del doc
            lista_indexada.append(i[0]) #Id palabra
            lista_indexada.append(get_llave(i[0]))#Palabra
            similares = obtener_similares(arreglo, i[0]) 
            lista_indexada.append(len(similares))#Frecuencia
            if len(similares) != 0:
                lista_indexada.append(1+math.log(len(similares),2))#TF
            else:
                lista_indexada.append(0)#TF
                
            lista_indexada.append(0)#IDF
            lista_indexada.append(0)#W

            lista_indexada.append(similares)#Posicion de apareciones
            palabras_visitadas.append(i[0])
            listas_indexadas.append(lista_indexada)
    for i in listas_indexadas:
        manejador_archivos.guardar_a_index(i)

#Obtiene las posiciones donde aparece los una palabra
def obtener_similares(arreglo, palabra):
    similares = []
    for i in arreglo:
        if i[0] == palabra:
            similares.append(i[1])
    return similares
#Obtiene la llave de un valor en un diccionario
def get_llave(val):
    for llave, valor in VOCABULARIO.items():
         if val == valor:
             return llave
#Una vez creado el index, calcula los idfs y actualiza el index y luego el peso
def calcular_idf_para_los_terminos():
    print("calculando...idfs...")
    index = manejador_archivos.obtener_index()
    palabras_vistadas = []
    palabras_nuevas = []
    nombre_archivos = os.listdir("base_datos/coleccion/preprocesado")
    for i in index:
        if i['palabra'] not in palabras_vistadas:
            ni = apariciones_en_coleccion(i['palabra'], index)
            Ni = len(nombre_archivos)
            idf = math.log(Ni/ni,2)
            palabra_y_cantidad = [i['palabra'],idf, idf*i['TF']]
            palabras_nuevas.append(palabra_y_cantidad)
            palabras_vistadas.append(i['palabra'])
    #Actualizando index y peso en el index
    print("act index...")
    for i in index:
        for j in palabras_nuevas:
            if i['palabra'] == j[0]:
                i['IDF'] = j[1]
                i['W'] = j[2]
        manejador_archivos.act_index(str(i)+'\n')

#Cuenta cuantas veces aparece una palabra en la colección
def apariciones_en_coleccion(palabra, coleccion):
    frecuencia = 0
    for i in coleccion:
        if i['palabra'] == palabra:
            frecuencia += 1
    return frecuencia

