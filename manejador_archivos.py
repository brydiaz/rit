import json
import ast


def obtener_valor(llave):
    with open('confg.json') as file:
        data = json.load(file)
        return data[llave]

def guardar_a_index(lista):
    indice = {
        "doc_name":lista[0],
        "id_palabra":lista[1],
        "palabra":lista[2],
        "frecuencia":lista[3],
        "TF":lista[4],
        "IDF":lista[5],
        "W":lista[6],
        "posiciones":lista[7]
    }
    with open('base_datos/index/index.txt', 'a') as file:
        file.write(str(indice) + '\n')


def obtener_index():
    f = open('base_datos/index/index.txt', 'r')
    palabra = f.readline()
    index_en_arreglo = []
    while palabra != "":
        str_to_dict = ast.literal_eval(palabra.strip())
        index_en_arreglo.append(str_to_dict)
        palabra = f.readline()
    return index_en_arreglo

def obtener_index_completo():
    f = open('base_datos/index/index_completo.txt', 'r')
    palabra = f.readline()
    index_en_arreglo = []
    while palabra != "":
        str_to_dict = ast.literal_eval(palabra.strip())
        index_en_arreglo.append(str_to_dict)
        palabra = f.readline()
    return index_en_arreglo

def act_index(linea):
    f = open('base_datos/index/index_completo.txt', 'a')
    f.write(linea)